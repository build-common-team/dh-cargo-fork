#!/usr/bin/perl
# debhelper sequence file for packaging Rust crates

use v5.36;
use strict;
use Debian::Debhelper::Dh_Lib;

# Build with rust buildsystem by default
add_command_options( "dh_auto_configure", "--buildsystem=rust" );
add_command_options( "dh_auto_build",     "--buildsystem=rust" );
add_command_options( "dh_auto_test",      "--buildsystem=rust" );
add_command_options( "dh_auto_install",   "--buildsystem=rust" );
add_command_options( "dh_auto_clean",     "--buildsystem=rust" );

# See https://bugs.debian.org/1023413
add_command_options( 'dh_clean', '-XCargo.toml.orig' );

1;
